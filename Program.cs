﻿Console.Write("Введите номер упражнения (1-4): ");
uint i = uint.Parse(Console.ReadLine());
Console.Write("Введите значение x: ");
double x = double.Parse(Console.ReadLine());
Console.Write("Введите значение y: ");
double y = double.Parse(Console.ReadLine());
double z;
if (i==1)
{
    z = Math.Pow(Math.Cos(x) - Math.Cos(y), 2.0)- Math.Pow(Math.Sin(x) - Math.Sin(y), 2.0);
    Console.WriteLine("Значение z = {0}", z);
}
if (i == 2)
{
    z = Math.Sin(4*x)/(1+Math.Cos(4*x))* Math.Cos(2 * y) / (1 + Math.Cos(2 * y));
    Console.WriteLine("Значение z = {0}", z);
}
if (i == 3)
{
    z =Math.Sqrt(Math.Pow(3*x+2,2.0)-24*x)/(3*Math.Sqrt(y)-2/Math.Sqrt(y)); ;
    Console.WriteLine("Значение z = {0}", z);
}
if (i == 4)
{
    z = ((x - 1) * Math.Sqrt(x)-(y-1)*Math.Sqrt(y))/(Math.Sqrt(Math.Pow(x,3.0)*y+x*y+Math.Pow(x,2.0)-x));
    Console.WriteLine("Значение z = {0}", z);
}

